<?php

return [
    'config_dir'  => 'novum.overheid',
    'namespace'   => 'NovumOverheid',
    'protocol'    => isset($_SERVER['IS_DEVEL']) ? 'http' : 'https',
    'live_domain' => 'overheid.demo.novum.nu',
    'dev_domain'  => 'overheid.demo.novum.nuidev.nl',
    'test_domain' => 'overheid.test.demo.novum.nu',
];
