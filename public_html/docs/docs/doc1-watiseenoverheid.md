---
id: doc1-watiseenoverheid
title: Introductie
sidebar_label: Wat is eenoverheid
---

Eenoverheid is een project ontwikkeld door [Novum](http://novum.nu), het innovatielab van de 
[Sociale verzekeringsbank](http://svb.nl). Het project bootst een digitale Nederlandse overheid na op een manier die het 
voor iedereen eenvoudig zou moeten maken om proof of concepts te lanceren.  

Het platform bestaat uit een aantal van elkaar gescheiden API's die elk een overheidsinstantie simuleren. De 
communicatie tussen de API's verloopt via het [NlX](https://nlx.io/) wat voldoen aan privacy wetgeving en de AVR moet 
vergemakkelijken. 

Bij de ontwikkeling van systeem zijn de principes van [common ground](https://commonground.nl/) zoveel mogelijk in acht 
genomen.
 
