---
id: doc2-kenmerken
title: Kenmerken
---

### Gegevens bij de bron
Gegevenstypen en validatie criteria worden centraal opgeslagen en beheert. Dit zorgt voor een verbetering in kwaliteit 
door consistentie.  

### AVG compatible
Alle API endpoints staan in principe volledig open te raadplegen maar het systeem automatiseerd het vastleggen van een 
grondslag en alle informatie opvragingen worden geregistreerd.


### Data-interoperabiliteit
Gegevenstypen en validatie criteria worden centraal opgeslagen en beheert. Dit zorgt voor een verbetering in kwaliteit 
door consistentie. Alle API endpoints zijn uniform van opzet en consistent ingericht wat gegevensuitwisseling 
vergemakkelijkt.  Door het gebruik van open software en open standaarden kunnen alle  applicaties en services, 
eenvoudig en veilig met elkaar communiceren op basis van moderne technieken. Dit verkleint voor overheden het risico op 
leveranciersafhankelijkheid.  

### Transparantie
Wetten en regels worden opgeslagen als taal agnostische logica die zowel kan dienen voor schematische weergave als voor 
toepassing zelf. Alle logica is via de API beschikbaar wat zorgt voor controleerbaarheid en eventuele derden de 
mogelijkheid bied om software te ontwikkelen die automatisch meebeweegt met wijzigingen in wet en regelgeving.

### API first
Door de gestandaardiseerde manier van het aanbieden van data in het API-first software landscape is het voor developers 
vanuit verschillende organisaties en leveranciers eenvoudig om data tussen organisaties uit te wisselen.


